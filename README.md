# Electoral Counter

* Quick Electoral Counter.
* Ready to use.
* Does not require a database, but relies on modern browsers capabilities like localStorage.

## Usage

### Basic Usage

After downloading, simply edit the HTML file and write down the names of the participants in the javascript portion of the conteo_ancianos.html file.

Use numeric keys (1-9) to increase the corresponding participant votes.

### Advanced Usage

(Still under development)
Use the CRUD functionality to manage your lists of participants.

## Bugs and Issues

Buts and issues will be controlled by the Issues section of this repository.

## About

This is just a quick couter made for personal use, if you are intending to use this piece of software on your own please improve and share as you wish.

## Copyright and License

Use as you wish under your own responsibility.
